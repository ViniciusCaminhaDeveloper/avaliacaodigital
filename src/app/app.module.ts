import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AvaliacaoDigitalComponent } from './avaliacao-digital/avaliacao-digital.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DropdownModule, DialogService, MessageService} from 'primeng/primeng';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ToastModule} from 'primeng/toast';
import { UnidadesModalComponent } from './Unidades/unidades-modal/unidades-modal.component';
import { ProfissionaisModalComponent } from './Unidades/profissionais-modal/profissionais-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    AvaliacaoDigitalComponent,
    UnidadesModalComponent,
    ProfissionaisModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CalendarModule,
    FormsModule,
    TableModule,ToastModule,
    DropdownModule,
    DynamicDialogModule,
    BrowserAnimationsModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.threeBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.0)', 
      primaryColour: 'gray',
      secondaryColour: 'gray',
      tertiaryColour : 'gray'
    })
  ],
  providers: [DialogService,MessageService, ],
  bootstrap: [AppComponent],
  entryComponents: [UnidadesModalComponent, ProfissionaisModalComponent],
})
export class AppModule { }
