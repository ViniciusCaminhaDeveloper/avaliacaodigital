import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AvaliacaoDigitalServiceService {

  constructor(private http: HttpClient) { }
  unidades;

  buscarPesquisas(): Observable<any> {
    return this.http.get('http://localhost:8000/buscarRankingDinamico/?filial=&especialidade=&profissional=');
  }

  rankingUnidades(): Observable<any> {
    return this.http.get('http://localhost:8000/rankingUnidades/')
  }

  rankingProfissionais(): Observable<any> {
    return this.http.get('http://localhost:8000/rankingProfissionais/')
  }

  buscarFiliais(): Observable<any> {
    return this.http.get('http://localhost:8000/buscarFiliais/')
  }

  buscarServico(filial): Observable<any> {
    return this.http.get('http://localhost:8000/servicos/?filial=' + filial)
  }

  buscarEspecialidade(filial): Observable<any> {
    return this.http.get('http://localhost:8000/especialidades/?filial=' + filial);
  }

  buscarRankingByFilial(filial): Observable<any> {
    return this.http.get('http://localhost:8000/rankingByFilial/?filial=' + filial);
  }

  buscarRankingProfissionaisByFilial(filial) {
    return this.http.get('http://localhost:8000/rankingProfissionaisByFilial/?filial=' + filial);

  }

  buscarRankingByFilialAndEspecialidade(filial, especialidade): Observable<any> {
    return this.http.get('http://localhost:8000/rankingByFilialAndEspecialidade/?filial=' + filial + '&especialidade=' + especialidade);
  }

  buscarRankingProfissionaisByFilialAndEspecialidade(filial, especialidade): Observable<any> {
    return this.http.get('http://localhost:8000/rankingProfissionaisByFilialAndEspecialidade/?filial=' + filial + '&especialidade=' + especialidade);
  }



  buscarRankingByFilialEspecialidadeProfissional(filial, especialidade, profissional) {
    return this.http.get('http://localhost:8000/rankingByFilialAndEspecialidadeAndProfissional/?filial=' + filial + '&especialidade=' + especialidade + '&profissional=' + profissional);

  }

  buscarProfissionaisRankingByFilialEspecialidadeProfissional(filial, especialidade, profissional) {
    return this.http.get('http://localhost:8000/rankingProfissionaisByFilialAndEspecialidadeAndProfissional/?filial=' + filial + '&especialidade=' + especialidade + '&profissional=' + profissional);
  }

  buscarTotalAvaliacoes(): Observable<any> {
    return this.http.get('http://localhost:8000/totalAvaliacoes/')
  }

  buscarProfissionais(filial, especialidade): Observable<any> {
    return this.http.get('http://localhost:8000/profissionais/?filial=' + filial + '&especialidade=' + especialidade)
  }

  buscarRankingByPeriodo(comeco, fim): Observable<any> {
    return this.http.get('http://localhost:8000/rankingByPeriodo/?comeco=' + comeco + '&fim=' + fim)
  }

  buscarRankingProfissionaisByPeriodo(comeco, fim): Observable<any> {
    return this.http.get('http://localhost:8000/rankingProfissionaisByPeriodo/?comeco=' + comeco + '&fim=' + fim)
  }

  buscarRankingByPeriodoAndFilial(comeco, fim, filial) {
    return this.http.get('http://localhost:8000/rankingByPeriodoAndFilial/?comeco=' + comeco + '&fim=' + fim + '&filial=' + filial)
  }

  buscarRankingProfissionaisByPeriodoAndFilial(comeco, fim, filial) {
    return this.http.get('http://localhost:8000/rankingProfissionaisByPeriodoAndFilial/?comeco=' + comeco + '&fim=' + fim + '&filial=' + filial)
  }

  buscarRankingByPeriodoAndFilialAndEspecialidade(comeco, fim, especialidade, filial) {
    return this.http.get('http://localhost:8000/rankingByPeriodoAndFilialAndEspecialidade/?comeco=' + comeco + '&fim=' + fim + '&especialidade=' + especialidade + '&filial=' + filial)
  }

  buscarRankingProfissionaisByPeriodoAndFilialAndEspecialidade(comeco, fim, especialidade, filial) {
    return this.http.get('http://localhost:8000/rankingProfissionaisByPeriodoAndFilialAndEspecialidade/?comeco=' + comeco + '&fim=' + fim + '&especialidade=' + especialidade + '&filial=' + filial)
  }

  buscarRankingByPeriodoAndFilialAndEspecialidadeAndProfissional(comeco, fim, especialidade, filial, profissional) {
    return this.http.get('http://localhost:8000/rankingByPeriodoAndFilialAndEspecialidadeAndProfissional/?comeco=' + comeco + '&fim=' + fim + '&especialidade=' + especialidade + '&filial=' + filial + '&profissional=' + profissional);
  }

  buscarProfissionaisRankingByFilialEspecialidadeProfissionalAndPeriodo(comeco, fim, especialidade, filial, profissional){
    return this.http.get('http://localhost:8000/rankingProfissionaisByPeriodoAndFilialAndEspecialidadeAndProfissional/?comeco=' + comeco + '&fim=' + fim + '&especialidade=' + especialidade + '&filial=' + filial + '&profissional=' + profissional);
  }

  enviaUnidadesService(value){
    this.unidades = value
  }

}
