import { TestBed } from '@angular/core/testing';

import { AvaliacaoDigitalServiceService } from './avaliacao-digital-service.service';

describe('AvaliacaoDigitalServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AvaliacaoDigitalServiceService = TestBed.get(AvaliacaoDigitalServiceService);
    expect(service).toBeTruthy();
  });
});
