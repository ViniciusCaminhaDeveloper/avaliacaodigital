import { Component, OnInit } from '@angular/core';
import { AvaliacaoDigitalServiceService } from '../services/avaliacao-digital-service.service';
import * as moment from 'moment';
import { DialogService } from 'primeng/primeng';
import { UnidadesModalComponent } from '../Unidades/unidades-modal/unidades-modal.component';
import { ProfissionaisModalComponent } from '../Unidades/profissionais-modal/profissionais-modal.component';



@Component({
  selector: 'app-avaliacao-digital',
  templateUrl: './avaliacao-digital.component.html',
  styleUrls: ['./avaliacao-digital.component.css']
})
export class AvaliacaoDigitalComponent implements OnInit {
  // VAR
  pesquisas;
  quantidadePeriodo = 0;
  totalAvaliacoes = 0;
  quantidadeTotal;
  periodo;

  //Rankings
  rankingUnidades: any;
  rankingUnidadesDecrescent = [];
  rankingProfissionais: any;
  rankingProfissionaisDecrescent = [];

  //Selects
  filialOptions = [];
  servicoOptions = [];
  especialidadeOptions = [];
  profissionaisOptions = [];

  // Loadings
  loadingAvaliacoes = true;
  loadingRanking = true;

  //SaveSelected
  filialSelected;
  especialidadeSelected;
  profissionalSelected;

  constructor(private avaliacaoDigitalService: AvaliacaoDigitalServiceService, public dialogService: DialogService) {
    this.buscaInicial();

    this.carregaRankingUnidades();
    this.carregaRankingProfissionais();

    this.carregaSelectFiliais();
  }

  ngOnInit() {
  }

  onChange(value: any) {
    this.filialSelected = value;
    if (this.periodo !== undefined) {
      this.servicoOptions = [];
      this.especialidadeOptions = [];
      this.profissionaisOptions = [];
      this.changePeriodo();
      this.avaliacaoDigitalService.buscarEspecialidade(value).subscribe(res => {
        res.forEach(element => {
          this.especialidadeOptions.push(element._id)
        });
      });
    }
    else {
      this.servicoOptions = [];
      this.especialidadeOptions = [];
      this.profissionaisOptions = [];

      this.avaliacaoDigitalService.buscarEspecialidade(value).subscribe(res => {
        res.forEach(element => {
          this.especialidadeOptions.push(element._id)
        });
      });


      this.avaliacaoDigitalService.buscarRankingByFilial(value).subscribe(res => {
        this.rankingUnidades = res;
        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.quantidadePeriodo = this.rankingUnidades[0].numAvaliacoes;
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)
      })

      this.avaliacaoDigitalService.buscarRankingProfissionaisByFilial(value).subscribe(res => {
        this.rankingProfissionais = res;
        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })
    }
  }

  changeEspecialidade(value) {
    if (this.periodo !== undefined) {
      this.profissionaisOptions = [];
    }
    this.avaliacaoDigitalService.buscarProfissionais(this.filialSelected, value).subscribe(res => {
      res.forEach(element => {
        this.profissionaisOptions.push(element._id)
      });
    });

    if (this.periodo !== undefined) {
      this.avaliacaoDigitalService.buscarRankingByPeriodoAndFilialAndEspecialidade(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.especialidadeSelected, this.filialSelected).subscribe(res => {
        this.rankingUnidades = res;
        this.quantidadePeriodo = this.rankingUnidades[0].numAvaliacoes
        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)

        this.especialidadeSelected = value;

        this.avaliacaoDigitalService.buscarRankingProfissionaisByPeriodoAndFilialAndEspecialidade(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.especialidadeSelected, this.filialSelected).subscribe(res => {
          this.rankingProfissionais = res;
          this.rankingProfissionais.forEach(element => {
            element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
          });
        })
      })
    }
    else {
      this.avaliacaoDigitalService.buscarRankingByFilialAndEspecialidade(this.filialSelected, value).subscribe(res => {
        this.rankingUnidades = res;
        this.quantidadePeriodo = this.rankingUnidades[0].numAvaliacoes
        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)

        this.especialidadeSelected = value;
      })

      this.avaliacaoDigitalService.buscarRankingProfissionaisByFilialAndEspecialidade(this.filialSelected, value).subscribe(res => {
        this.rankingProfissionais = res;
        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })


    }
  }

  changeProfissional(value) {
    if (this.periodo == undefined) {
      this.avaliacaoDigitalService.buscarRankingByFilialEspecialidadeProfissional(this.filialSelected, this.especialidadeSelected, value).subscribe(res => {
        this.rankingUnidades = res;

        this.quantidadePeriodo = this.rankingUnidades[0].numAvaliacoes
        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)
      })

      this.avaliacaoDigitalService.buscarProfissionaisRankingByFilialEspecialidadeProfissional(this.filialSelected, this.especialidadeSelected, value).subscribe(res => {
        this.rankingProfissionais = res;

        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })
    }
    else {
      this.avaliacaoDigitalService.
        buscarRankingByPeriodoAndFilialAndEspecialidadeAndProfissional(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.especialidadeSelected, this.filialSelected, value).subscribe(res => {
          this.rankingUnidades = res;

          this.quantidadePeriodo = this.rankingUnidades[0].numAvaliacoes
          this.rankingUnidades.forEach(element => {
            element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
          });
          this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)
        })

      this.avaliacaoDigitalService.buscarProfissionaisRankingByFilialEspecialidadeProfissionalAndPeriodo(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.especialidadeSelected, this.filialSelected, value).subscribe(res => {
        this.rankingProfissionais = res;

        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })
    }
  }

  changePeriodo() {
    if (this.filialSelected === undefined) {
      this.avaliacaoDigitalService.buscarRankingByPeriodo(this.periodo[0].toISOString(), this.periodo[1].toISOString()).subscribe(res => {
        this.rankingUnidades = res;
        var count = 0;
        if (this.rankingUnidades.length == 0) {
          this.quantidadePeriodo = 0
        }
        else {
          this.rankingUnidades.forEach(element => {
            count = count + element.numAvaliacoes
            this.quantidadePeriodo = count
          });
        }

        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)
      })

      this.avaliacaoDigitalService.buscarRankingProfissionaisByPeriodo(this.periodo[0].toISOString(), this.periodo[1].toISOString()).subscribe(res => {
        this.rankingProfissionais = res;

        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })
    }
    if (this.filialSelected !== undefined) {
      this.avaliacaoDigitalService.buscarRankingByPeriodoAndFilial(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.filialSelected).subscribe(res => {
        this.rankingUnidades = res;
        var count = 0;
        if (this.rankingUnidades.length == 0) {
          this.quantidadePeriodo = 0
        }
        else {
          this.rankingUnidades.forEach(element => {
            count = count + element.numAvaliacoes
            this.quantidadePeriodo = count
          });
        }

        this.rankingUnidades.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
        this.totalAvaliacoes = Number((this.quantidadePeriodo / this.quantidadeTotal) * 100)
      })

      this.avaliacaoDigitalService.buscarRankingProfissionaisByPeriodoAndFilial(this.periodo[0].toISOString(), this.periodo[1].toISOString(), this.filialSelected).subscribe(res => {
        this.rankingProfissionais = res;

        this.rankingProfissionais.forEach(element => {
          element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
        });
      })
    }

    if (this.filialSelected !== undefined && this.especialidadeSelected !== undefined) {
      this.changeEspecialidade(this.especialidadeSelected);
    }
  }

  buscaInicial() {
    this.avaliacaoDigitalService.buscarPesquisas().subscribe(res => {
      this.pesquisas = res;
      this.loadingAvaliacoes = false;
      this.avaliacaoDigitalService.buscarTotalAvaliacoes().subscribe(res => {
        this.totalAvaliacoes = ((res.total / res.total) * 100)
        this.quantidadeTotal = res.total
        this.quantidadePeriodo = res.total
      })
    });
  }

  carregaRankingUnidades() {
    this.avaliacaoDigitalService.rankingUnidades().subscribe(res => {
      this.rankingUnidades = res;
      this.rankingUnidades.forEach(element => {
        element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
      });
      this.loadingRanking = false;

    });
  }

  carregaRankingProfissionais() {
    this.avaliacaoDigitalService.rankingProfissionais().subscribe(res => {
      this.rankingProfissionais = res;
      this.rankingProfissionais.forEach(element => {
        element.avgClassificacao = element.avgClassificacao.toString().substring(0, 1)
      });
      this.loadingRanking = false;
    });
  }

  carregaSelectFiliais() {
    this.avaliacaoDigitalService.buscarFiliais().subscribe(res => {
      res.forEach(element => {
        this.filialOptions.push(element.nome);
      });
    });
  }

  showUnidades() {
    const ref = this.dialogService.open(UnidadesModalComponent, {
      data: this.rankingUnidades,
      header: 'UNIDADES',
      width: '70%',
      contentStyle: { "max-height": "350px", "overflow": "auto" }
    });
    this.avaliacaoDigitalService.enviaUnidadesService(this.rankingUnidades);
    
  }

  showProfissionais() {
    const ref = this.dialogService.open(ProfissionaisModalComponent, {
      data: this.rankingUnidades,
      header: 'Profissionais',
      width: '70%',
      contentStyle: { "max-height": "350px", "overflow": "auto" }
    });
    this.avaliacaoDigitalService.enviaUnidadesService(this.rankingProfissionais);
    
  }




  colorchange(id) {
    let tabs = ['pessimo-tab', 'ruim-tab', 'podeMelhorar-tab', 'bom-tab', 'adorei-tab']

    var backgroundColor = document.getElementById(id).style.backgroundColor;

    if (backgroundColor == "white") {
      document.getElementById(id).style.backgroundColor = "rgb(242, 242, 242)";

      for (var i = 0; i < tabs.length; i++) {
        if (tabs[i] !== id) {
          document.getElementById(tabs[i]).style.backgroundColor = "white";
        }
      }
    }
  }

}
