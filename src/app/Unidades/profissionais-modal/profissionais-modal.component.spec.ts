import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfissionaisModalComponent } from './profissionais-modal.component';

describe('ProfissionaisModalComponent', () => {
  let component: ProfissionaisModalComponent;
  let fixture: ComponentFixture<ProfissionaisModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfissionaisModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfissionaisModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
