import { Component, OnInit } from '@angular/core';
import { AvaliacaoDigitalServiceService } from 'src/app/services/avaliacao-digital-service.service';


@Component({
  selector: 'app-profissionais-modal',
  templateUrl: './profissionais-modal.component.html',
  styleUrls: ['./profissionais-modal.component.css']
})
export class ProfissionaisModalComponent implements OnInit {
  rankingUnidades = [];
  constructor(public avaliacaoService : AvaliacaoDigitalServiceService ) {
    this.rankingUnidades = this.avaliacaoService.unidades;
   }

  ngOnInit() {
  }

}
