import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadesModalComponent } from './unidades-modal.component';

describe('UnidadesModalComponent', () => {
  let component: UnidadesModalComponent;
  let fixture: ComponentFixture<UnidadesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
