import { Component, OnInit, Input } from '@angular/core';
import { AvaliacaoDigitalServiceService } from 'src/app/services/avaliacao-digital-service.service';


@Component({
  selector: 'app-unidades-modal',
  templateUrl: './unidades-modal.component.html',
  styleUrls: ['./unidades-modal.component.css']
})
export class UnidadesModalComponent implements OnInit {

  rankingUnidades = [];
  constructor(private avaliacaoService : AvaliacaoDigitalServiceService) {
      this.rankingUnidades = this.avaliacaoService.unidades;
   }

  ngOnInit() {
  }

}
