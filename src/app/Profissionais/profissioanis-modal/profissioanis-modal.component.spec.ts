import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfissioanisModalComponent } from './profissioanis-modal.component';

describe('ProfissioanisModalComponent', () => {
  let component: ProfissioanisModalComponent;
  let fixture: ComponentFixture<ProfissioanisModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfissioanisModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfissioanisModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
